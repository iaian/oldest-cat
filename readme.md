### DDFL oldest cat

This is a FP learning project repo, based heavily on [https://github.com/lexiross/fattest-cat](https://github.com/lexiross/fattest-cat)

The functional approach is based upon lodash FP pipe, as shown here : [https://medium.com/javascript-scene/master-the-javascript-interview-what-is-function-composition-20dfb109a1a0](https://medium.com/javascript-scene/master-the-javascript-interview-what-is-function-composition-20dfb109a1a0)

## To run / dev

* `$ npm install`
* `$ node index.js`
