#!/usr/bin/env node

const _ = require('lodash');
const request = require('request-promise');
const Promise = require('bluebird');
const cheerio = require('cheerio');
const colors = require('colors');
const present = require('present');

// Lodash FP utils
const pipe = require('lodash/fp/pipe');
const curry = require('lodash/fp/curry');
const map = require('lodash/fp/map');
const reduce = require('lodash/fp/reduce');
const filter = require('lodash/fp/filter');
const sortBy = require('lodash/fp/sortBy');

const ADOPTION_PAGE = `http://petharbor.com/results.asp?searchtype=ADOPT&start=4&nomax=1&rows=500&imght=80&imgres=thumb&view=sysadm.v_ddfl&amp;friends=0&amp;nobreedreq=1&amp;nowidensearch=1&amp;notitle=1&amp;nosuccess=1&nopod=1&shelterlist=%27DDFL%27,%27DDFL1%27,%27DDFL2%27&WHERE=type_CAT`;

const textSeparator = '-----------------------------------------------------------------------'.rainbow.bold;

// Array of table titles
// TODO refactor to not use a global let
let tableTitles = [];

// For storing timer values
let timers = {
	requestTimer: 0,
	dataProcessingTimer: 0,
}


// Utils
//

// For introspection inside of a pipe
const trace = curry((label, x) => {
	console.log(`== ${ label }:`, x);
	return x;
});

// Cleanup format of strings for use as JSON key(s)
const makeStringJsonFriendly = (string) => {
	let newString = string;
	newString = newString.toLowerCase();

	return newString;
}

// Return the table headers from scrape data
const getTableText = (obj) => {
	return obj.children[0].data;
}

// Remove table headings from scrape data
const removeTableHeadings = (obj) => {
	return obj.children[0].attribs.class !== 'TableTitle'
}

// Filter out all un-needed data from scrape data
const removeInvalidChildData = (obj) => {
	return obj.children.filter((tdTag) => {
		return tdTag.children[0].data;
	})
}

// Reduce scrape array of table rows data to catData object
const transformToCatObject = (trTag) => {
	return trTag.reduce((catData, tdTag, index) => {
		const propertyName = tableTitles[index];
		const thisValue = tdTag.children[0].data;

		catData[propertyName] = thisValue;

		return catData;
	}, {});
}

// Cleanup age field data and convert to type number
const fixCatAgeField = (obj) => {
	let newAge = obj.age;

	newAge = newAge.toLowerCase();
	newAge = newAge.replace('years', '');
	newAge = parseInt(newAge);

	obj.age = newAge;

	return obj;
}

// Display cat data to user
const listCats = (catData) => {
	console.log(
		`${catData.name} a ${catData['main color']} ${catData.breed} - age`.dim,
		`${catData.age}`.dim.underline
	);

	return catData;
}


// Composed FP functions
//

// Build array of table titles
const getTableTitleArray = pipe(
	map(getTableText),
	map(makeStringJsonFriendly)
);

// Build array of cat data
const getCatDataArray = pipe(
	filter(removeTableHeadings),
	map(removeInvalidChildData),
	map(transformToCatObject),
	map(fixCatAgeField),
	sortBy(['age'])
);


// Main
const fetchCatsMain = Promise.method((catsSoFar) => {
	console.log("Accessing Denver DFL - cats listing ...");
	
	let requestStart = present(); // For request timing
	
	return request.get(ADOPTION_PAGE)
		.then((adoptionsPage) => {
			
			console.log('- response recieved -');
			
			// Timing request
			let requestEnd = present();
			timers.requestTimer = (requestEnd - requestStart).toFixed(3);
			// Timing processing
			let processingStart = present();
			
			// Cheerio page scrape helper variables
			const adoptionsPageScrape = cheerio(adoptionsPage);
			const adoptionPageTableHeaders = adoptionsPageScrape.find('table.ResultsTable tr td.TableTitle a');
			const adoptionPageTableRows = adoptionsPageScrape.find('table.ResultsTable tr')

			// Set table titles array
			// TODO refactor to not use a global let
			tableTitles = getTableTitleArray(adoptionPageTableHeaders);
			
			// Array of catData objects
			const cats = getCatDataArray(adoptionPageTableRows);
			
			// Helper variables for display to user
			const oldest = _(cats).map('age').max();
			const oldestCats = _.filter(cats, {
				age: oldest
			});
			const names = _.map(oldestCats, 'name');
			const tie = oldestCats.length > 1;
			const nameText = (tie ? `${names.slice(0, -1).join(", ")} and ${_.last(names)}` : names[0]);
			
			// Timing processing
			let processingEnd = present();
			timers.dataProcessingTimer = (processingEnd - processingStart).toFixed(3);

			// Below are items for display to user
			//
			
			console.log('Sorted from youngest to oldest: ');
			console.log(textSeparator);

			// List all cats
			cats.map(listCats);

			console.log(textSeparator);
			console.log(
				`Total number of cats at Denver DFL:`.cyan.bold,
				`${cats.length}`.cyan.bold.underline
			);

			console.log(
				colors.yellow.bold(
					(tie ? 'Oldest cats at Denver DFL are:' : 'Oldest cat at Denver DFL is:')
				),
				colors.green.underline.bold(nameText),
				colors.yellow.bold('at the ripe age of'),
				colors.green.underline.bold(oldest)
			);
			
			console.log(textSeparator);
			
			console.log(`request took: ${timers.requestTimer}ms`);
			console.log(`processing cat data took: ${timers.dataProcessingTimer}ms`);
		});
});

// Execute main
fetchCatsMain(0, []);
